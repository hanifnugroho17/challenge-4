import React, {useEffect} from 'react';
import {StyleSheet, View, Text, Image} from 'react-native';
import {WARNA_SEKUNDER, WARNA_UTAMA} from '../utils/constant';

const Splash = ({navigation}) => {
  useEffect(() => {
    setTimeout(() => {
      navigation.navigate('Login');
    }, 2000);
  }, [navigation]);

  return (
    <View style={styles.container}>
      <Image style={styles.Logo} source={require('../assets/logo.png')} />
      <Text style={styles.name}>Hanif Nugroho Jati</Text>
    </View>
  );
};

export default Splash;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: WARNA_UTAMA,
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  Logo: {
    width: 200,
    height: 200,
    borderRadius: 20,
  },
  name: {
    color: WARNA_SEKUNDER,
    fontWeight: 'bold',
    fontSize: 18,
  },
});
